import React from 'react'
import footerStyles from '../styles/Footer.module.scss'
function Footer() {
    return (
        <div className={footerStyles.footer}>
        <h3>UgTours@2021</h3>
        <h5>All rigts reserved.</h5>
        
        </div>
    )
}

export default Footer
